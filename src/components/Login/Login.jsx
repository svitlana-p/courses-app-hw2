import { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import { AuthContext } from '../../context/AuthContext';

import { useHttp } from '../../hooks/useHttp';

import './Login.css';

const Login = () => {
	const auth = useContext(AuthContext);
	const { loading, error, request } = useHttp();
	const [message, setMessage] = useState(null);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [emailDirty, setEmailDirty] = useState(false);
	const [passwordDirty, setPasswordDirty] = useState(false);
	const [emailError, setEmailEror] = useState('Email should not be empty');
	const [passwordError, setPasswordEror] = useState(
		'Password should not be empty'
	);
	const [formValid, setFormValid] = useState(false);

	useEffect(() => {
		setMessage(error);
	}, [error]);

	useEffect(() => {
		if (emailError || passwordError) {
			setFormValid(false);
		} else {
			setFormValid(true);
		}
	}, [emailError, passwordError]);

	const onEmailChanged = (e) => {
		setEmail(e.target.value);
		const reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		if (!reg.test(e.target.value.toString().toLowerCase())) {
			setEmailEror('Please, enter corect email');
		} else {
			setEmailEror('');
		}
	};

	const onPasswordChanged = (e) => {
		setPassword(e.target.value);
		const reg = /^(?=.*[0-9])(?=.*[a-z])(?=\S+$).{8,}$/;
		if (!reg.test(e.target.value.toString().toLowerCase())) {
			setPasswordEror(
				'Password should contain at least one lower case letter and be more than 7 characters'
			);
		} else {
			setPasswordEror('');
		}
	};

	const blurHandler = (e) => {
		switch (e.target.name) {
			case 'email':
				setEmailDirty(true);
				break;
			case 'password':
				setPasswordDirty(true);
				break;
			default:
				break;
		}
	};

	const formSubmit = async (event) => {
		const newUser = {
			password,
			email,
		};
		event.preventDefault();
		try {
			const data = await request(`/login`, 'POST', { ...newUser });
			const token = data.result.substring(7);
			const username = data.user.name;
			auth.login(token, username);
		} catch (e) {}
	};
	return (
		<div className='login__container'>
			<div className='login'>
				<h2 className='login__header login__text'>Login</h2>
				<form className='login__form' onSubmit={formSubmit}>
					<div className='login__input'>
						<Input
							labelText='Email'
							placeholderText='Enter email'
							type='email'
							value={email}
							name='email'
							onChange={onEmailChanged}
							onBlur={blurHandler}
						/>
						{emailDirty && emailError && (
							<div className='error'>{emailError}</div>
						)}
					</div>
					<div className='login__input'>
						<Input
							labelText='Password'
							placeholderText='Enter password'
							type='password'
							value={password}
							name='password'
							onChange={onPasswordChanged}
							onBlur={blurHandler}
						/>
						{passwordDirty && passwordError && (
							<div className='error'>{passwordError}</div>
						)}
					</div>
					<div className='login__btn login__text'>
						<Button
							type='submit'
							buttonText='Login'
							disabled={!formValid || loading}
						/>
					</div>
					<div className='error'>{message ? message.message : null}</div>
				</form>
				<div className='login__text'>
					<span>If don't you have an account you can </span>
					<Link to='/registration'>Sign Up</Link>
				</div>
			</div>
		</div>
	);
};

export default Login;
