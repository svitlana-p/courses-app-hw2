import './Header.css';
import Button from '../../common/Button/Button';
import Logo from './components/Logo/Logo';

import { LOGOUT_BUTTON_TEXT } from '../../constants.js';
import { useContext } from 'react';
import { AuthContext } from '../../context/AuthContext';

const Header = () => {
	const auth = useContext(AuthContext);

	const logout = () => {
		auth.logout();
	};

	return (
		<header className='header'>
			<Logo />
			{auth.isAuthenticated ? (
				<div className='header__container'>
					<p className='header__text'>{auth.username}</p>
					<Button buttonText={LOGOUT_BUTTON_TEXT} onClick={logout} />
				</div>
			) : null}
		</header>
	);
};
export default Header;
