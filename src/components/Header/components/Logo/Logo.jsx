import './Logo.css';

const Logo = () => {
	return (
		<img
			className='logo__img'
			src='https://i.ibb.co/mqhBLLt/2022-12-23.png'
			alt='logo'
		></img>
	);
};
export default Logo;
