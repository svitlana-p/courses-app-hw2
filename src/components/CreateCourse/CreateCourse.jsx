import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import Textarea from '../../common/Textarea/Textarea';
import {
	mockedAuthorsList,
	mockedCoursesList,
	CREATE_BUTTON_TEXT,
	CREATE_AUTHOR_BUTTON_TEXT,
	ADD_AUTHOR_BUTTON_TEXT,
	DELETE_AUTHOR_BUTTON_TEXT,
} from '../../constants';

import { pipeDuration } from '../../helpers/pipeDuration.js';
import { uniqueId } from '../../helpers/uniqueIdGenerator';

import './CreateCourse.css';

const CreateCourse = () => {
	const [authors, setAuthors] = useState(mockedAuthorsList);
	const [title, setTitle] = useState('');
	const [descriprion, setDescription] = useState('');
	const [author, setAuthor] = useState('');
	const [duration, setDuration] = useState('');
	const [chosenAuthors, setChosenAuthors] = useState([]);

	const navigate = useNavigate();

	const onTitleChange = (e) => {
		setTitle(e.target.value);
	};

	const onDescriptionChange = (e) => {
		setDescription(e.target.value);
	};

	const onAuthorChange = (e) => {
		setAuthor(e.target.value);
	};

	const onAuthorAdded = () => {
		if (author.length < 2) {
			alert('Author name should be at least 2 characters');
			return;
		}
		const newAuthor = {
			id: uniqueId(),
			name: author,
		};
		setAuthors([...authors, newAuthor]);
		mockedAuthorsList.push(newAuthor);
		setAuthor('');
	};
	const onDurationChange = (e) => {
		setDuration(e.target.value);
	};
	const onAuthorChosen = (author) => {
		setChosenAuthors([...chosenAuthors, author]);
		setAuthors((authors) => authors.filter((item) => item.id !== author.id));
	};
	const onAuthorDeleted = (author) => {
		setChosenAuthors((chosenAuthors) =>
			chosenAuthors.filter((item) => item.id !== author.id)
		);
		setAuthors([...authors, author]);
	};

	const onCourseCreate = () => {
		if (
			title.length < 2 ||
			descriprion.length < 2 ||
			!chosenAuthors ||
			!duration
		) {
			alert('Please, fill in all fields ');
			return;
		}
		const date = new Date().toLocaleDateString();
		const courseAuthors = chosenAuthors.map((author) => author.id);
		const newCourse = {
			id: uniqueId(),
			title: title,
			description: descriprion,
			creationDate: date,
			duration: duration,
			authors: courseAuthors,
		};
		mockedCoursesList.push(newCourse);
		setTitle('');
		setDescription('');
		setDuration('');
		navigate('/courses');
	};

	return (
		<div className='course-container'>
			<div className='course-container__toolbar'>
				<Input
					labelText='Title'
					placeholderText='Enter title...'
					onChange={onTitleChange}
					value={title}
				/>
				<Button buttonText={CREATE_BUTTON_TEXT} onClick={onCourseCreate} />
			</div>
			<Textarea
				labelText='Description'
				placeholderText='Enter description'
				onChange={onDescriptionChange}
				value={descriprion}
			/>
			<div className='course-container__authors'>
				<div className='course-container__author-info'>
					<p className='course-title'>Add author</p>

					<div className='course-input'>
						<Input
							labelText='Author name'
							placeholderText='Enter author name...'
							onChange={onAuthorChange}
							value={author}
						/>
					</div>
					<div className='course-btn'>
						<Button
							buttonText={CREATE_AUTHOR_BUTTON_TEXT}
							onClick={onAuthorAdded}
						/>
					</div>

					<p className='course-title'>Duration</p>
					<div className='course-input'>
						<Input
							type='number'
							labelText='Duration'
							placeholderText='Enter duration in minutes...'
							onChange={onDurationChange}
							value={duration}
						/>
						<p>
							Duration:
							<span className='course-duration'>
								{duration ? pipeDuration(duration) : null}{' '}
							</span>{' '}
							hours
						</p>
					</div>
				</div>
				<div className='course-container__author-selection'>
					<p className='course-title'>Authors</p>
					<ul className='course__authors'>
						{authors.length !== 0 ? (
							authors.map((author) => {
								return (
									<li className='course__author' key={author.id}>
										<p>{author.name}</p>
										<Button
											buttonText={ADD_AUTHOR_BUTTON_TEXT}
											onClick={() => onAuthorChosen(author)}
										/>
									</li>
								);
							})
						) : (
							<p className='course-authors'>There are no authors</p>
						)}
					</ul>
					<p className='course-title'>Course authors</p>
					<ul className='course__authors'>
						{chosenAuthors.length !== 0 ? (
							chosenAuthors.map((author) => {
								return (
									<li className='course__author' key={author.id}>
										<p>{author.name}</p>
										<Button
											buttonText={DELETE_AUTHOR_BUTTON_TEXT}
											onClick={() => onAuthorDeleted(author)}
										/>
									</li>
								);
							})
						) : (
							<p className='course-authors'>Authors list is empty</p>
						)}
					</ul>
				</div>
			</div>
		</div>
	);
};

export default CreateCourse;
