import { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';

import { useHttp } from '../../hooks/useHttp';

import './Registration.css';

const Registration = () => {
	const { loading, error, request } = useHttp();
	const [message, setMessage] = useState(null);
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [nameDirty, setNameDirty] = useState(false);
	const [emailDirty, setEmailDirty] = useState(false);
	const [passwordDirty, setPasswordDirty] = useState(false);
	const [nameError, setNameEror] = useState('Name should not be empty');
	const [emailError, setEmailEror] = useState('Email should not be empty');
	const [passwordError, setPasswordEror] = useState(
		'Password should not be empty'
	);
	const [formValid, setFormValid] = useState(false);

	let navigate = useNavigate();

	useEffect(() => {
		setMessage(error);
	}, [error]);

	useEffect(() => {
		if (nameError || emailError || passwordError) {
			setFormValid(false);
		} else {
			setFormValid(true);
		}
	}, [nameError, emailError, passwordError]);

	const onNameChanged = (e) => {
		setName(e.target.value);
		if (name.length < 1) {
			setNameEror('Name should be more than 2 characters');
		} else {
			setNameEror('');
		}
	};

	const onEmailChanged = (e) => {
		setEmail(e.target.value);
		const reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		if (!reg.test(e.target.value.toString().toLowerCase())) {
			setEmailEror('Please, enter corect email');
		} else {
			setEmailEror('');
		}
	};

	const onPasswordChanged = (e) => {
		setPassword(e.target.value);
		const reg = /^(?=.*[0-9])(?=.*[a-z])(?=\S+$).{8,}$/;
		if (!reg.test(e.target.value.toString().toLowerCase())) {
			setPasswordEror(
				'Password should contain at least one lower case letter and be more than 7 characters'
			);
		} else {
			setPasswordEror('');
		}
	};

	const blurHandler = (e) => {
		switch (e.target.name) {
			case 'name':
				setNameDirty(true);
				break;
			case 'email':
				setEmailDirty(true);
				break;
			case 'password':
				setPasswordDirty(true);
				break;
			default:
				break;
		}
	};

	const formSubmit = async (event) => {
		const newUser = {
			name: name,
			email: email,
			password: password,
		};
		event.preventDefault();
		try {
			await request(`/register`, 'POST', { ...newUser });
			navigate('/login');
		} catch (e) {}
	};

	return (
		<div className='registration__container'>
			<div className='registration'>
				<h2 className='registration__header registration__text'>
					Registration
				</h2>
				<form className='registration__form' onSubmit={formSubmit}>
					<div className='registration__input'>
						<Input
							labelText='Name'
							placeholderText='Enter name'
							value={name}
							name='name'
							onChange={onNameChanged}
							onBlur={blurHandler}
						/>
						{nameDirty && nameError && <div className='error'>{nameError}</div>}
					</div>
					<div className='registration__input '>
						<Input
							labelText='Email'
							placeholderText='Enter email'
							type='email'
							value={email}
							name='email'
							onChange={onEmailChanged}
							onBlur={blurHandler}
						/>
						{emailDirty && emailError && (
							<div className='error'>{emailError}</div>
						)}
					</div>
					<div className='registration__input'>
						<Input
							labelText='Password'
							placeholderText='Enter password'
							type='password'
							value={password}
							name='password'
							onChange={onPasswordChanged}
							onBlur={blurHandler}
						/>
						{passwordDirty && passwordError && (
							<div className='error'>{passwordError}</div>
						)}
					</div>
					<div className='registration__btn registration__text'>
						<Button
							disabled={!formValid || loading}
							type='submit'
							buttonText='Registration'
						/>
					</div>
					<div className='error'>{message ? message.message : null}</div>
				</form>
				<div className='registration__text'>
					<span>If you have an account you can </span>
					<Link to='/login'>Login</Link>
				</div>
			</div>
		</div>
	);
};

export default Registration;
