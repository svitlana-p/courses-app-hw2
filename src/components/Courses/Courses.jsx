import { useState } from 'react';

import {
	mockedCoursesList,
	mockedAuthorsList,
	ADD_BUTTON_TEXT,
} from '../../constants.js';

import Button from '../../common/Button/Button.jsx';
import CourseCard from './components/CourseCard/CourseCard.jsx';
import SearchBar from './components/SearchBar/SearchBar.jsx';

import './Courses.css';
import { useNavigate } from 'react-router-dom';

const Courses = () => {
	const [term, setTerm] = useState('');
	const navigate = useNavigate();
	const searchCourse = (items, term) => {
		if (term.length === 0) return items;
		return items.filter((item) => {
			return (
				item.title.toLowerCase().indexOf(term) > -1 ||
				item.id.toLowerCase().indexOf(term) > -1
			);
		});
	};
	const onUpdateSearch = (term) => {
		setTerm(term);
	};
	const visibleCourses = searchCourse(mockedCoursesList, term);
	const getCourseAuthors = (authorsIds) => {
		return authorsIds.map((authorId, index) => {
			const author = mockedAuthorsList.find((author) => author.id === authorId);

			if (!author) return null;

			if (index === authorsIds.length - 1) return author.name;
			return author.name;
		});
	};
	const onCourseAdd = () => {
		navigate('/courses/add');
	};

	return (
		<div className='container'>
			<>
				<div className='container__toolbar'>
					<SearchBar onUpdateSearch={onUpdateSearch} />
					<Button buttonText={ADD_BUTTON_TEXT} onClick={onCourseAdd} />
				</div>
				<ul className='container__list'>
					{visibleCourses.map((course) => {
						const authors = getCourseAuthors(course.authors).join(', ');
						return (
							<CourseCard
								key={course.id}
								id={course.id}
								title={course.title}
								duration={course.duration}
								creationDate={course.creationDate}
								description={course.description}
								authors={authors}
							/>
						);
					})}
				</ul>
			</>
		</div>
	);
};

export default Courses;
