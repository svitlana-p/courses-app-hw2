import { useNavigate } from 'react-router-dom';

import Button from '../../../../common/Button/Button.jsx';
import { dateGenerator } from '../../../../helpers/dateGenerator.js';
import { pipeDuration } from '../../../../helpers/pipeDuration.js';

import { SHOW_BUTTON_TEXT } from '../../../../constants.js';

import './CourseCard.css';

const CourseCard = (props) => {
	const { id, title, duration, creationDate, description, authors } = props;
	const navigate = useNavigate();

	const showCourse = () => {
		navigate(`/courses/${id}`);
	};

	return (
		<li className='item__container'>
			<div className='item__text'>
				<div className='item__title'>{title}</div>
				<div className='item__desc'>{description}</div>
			</div>
			<div className='item__description'>
				<div className='item__field'>
					<span className='item__field--bold'>Authors: </span>
					{authors}
				</div>
				<div className='item__field'>
					<span className='item__field--bold'>Duration: </span>
					{pipeDuration(duration)}
				</div>
				<div className='item__field'>
					<span className='item__field--bold'>Created: </span>
					{dateGenerator(creationDate)}
				</div>
				<div className='item__btn'>
					<Button buttonText={SHOW_BUTTON_TEXT} onClick={showCourse} />
				</div>
			</div>
		</li>
	);
};

export default CourseCard;
