import { BrowserRouter as Router } from 'react-router-dom';

import { useRoutes } from './hooks/useRoutes';

import Header from './components/Header/Header';
import { AuthContext } from './context/AuthContext';
import { useAuth } from './hooks/useAuth';

function App() {
	const { token, login, logout, username } = useAuth();
	const isAuthenticated = !!token;
	const routes = useRoutes(isAuthenticated);
	return (
		<AuthContext.Provider
			value={{
				token,
				login,
				logout,
				username,
				isAuthenticated,
			}}
		>
			<Router>
				<Header />
				{routes}
			</Router>
		</AuthContext.Provider>
	);
}
export default App;
