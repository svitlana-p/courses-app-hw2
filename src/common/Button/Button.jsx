import './Button.css';

const Button = ({ buttonText, onClick, type = 'button', disabled }) => {
	return (
		<button className='btn' onClick={onClick} type={type} disabled={disabled}>
			{buttonText}
		</button>
	);
};
export default Button;
