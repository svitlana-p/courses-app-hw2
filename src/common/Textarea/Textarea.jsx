import './Textarea.css';

const Textarea = ({ labelText, placeholderText, onChange, value }) => {
	return (
		<div className='textarea-container'>
			<label htmlFor='text'>{labelText}</label>
			<textarea
				className='textarea'
				onChange={onChange}
				rows='5'
				name='text'
				placeholder={placeholderText}
				value={value}
			></textarea>
		</div>
	);
};

export default Textarea;
