import './Input.css';

const Input = ({
	labelText,
	placeholderText,
	onChange,
	onBlur,
	type = 'text',
	name,
	value,
}) => {
	return (
		<div className='form-input'>
			<label htmlFor='input'>{labelText}</label>
			<input
				className='input'
				onChange={onChange}
				onBlur={onBlur}
				type={type}
				name={name}
				placeholder={placeholderText}
				value={value}
			></input>
		</div>
	);
};

export default Input;
