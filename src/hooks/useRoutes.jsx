import { lazy, Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';

const Courses = lazy(() => import('../components/Courses/Courses'));
const Registration = lazy(() =>
	import('../components/Registration/Registration')
);
const Login = lazy(() => import('../components/Login/Login'));
const CourseInfo = lazy(() => import('../components/CourseInfo/CourseInfo'));
const CreateCourse = lazy(() =>
	import('../components/CreateCourse/CreateCourse')
);

export const useRoutes = (isAuthenticated) => {
	if (isAuthenticated) {
		return (
			<main>
				<Suspense fallback={<p>Loading...</p>}>
					<Routes>
						<Route path='/' element={<Courses />} />
						<Route path='/courses' element={<Courses />} />
						<Route path='/courses/add' element={<CreateCourse />} />
						<Route path='/courses/:courseId' element={<CourseInfo />} />
						<Route path='*' element={<Courses />} />
					</Routes>
				</Suspense>
			</main>
		);
	}

	return (
		<main>
			<Suspense fallback={<p>Loading...</p>}>
				<Routes>
					<Route path='/' element={<Login />} />
					<Route path='/registration' element={<Registration />} />
					<Route path='/login' element={<Login />} />
					<Route path='*' element={<Login />} />
				</Routes>
			</Suspense>
		</main>
	);
};
