import { useCallback, useState } from 'react';

export const useHttp = () => {
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(null);

	const request = useCallback(
		async (url, method = 'GET', body, headers = {}) => {
			setLoading(true);

			if (body) {
				headers['Content-Type'] = 'application/json';
			}

			try {
				const response = await fetch(url, {
					method,
					body: body && JSON.stringify(body),
					headers,
				});
				const data = await response.json();
				setError(null);

				if (!response.ok) {
					throw new Error(data.errors || 'Something went wrong');
				}
				setLoading(false);
				return data;
			} catch (error) {
				setLoading(false);
				setError(error);
				throw error;
			}
		},
		[]
	);

	return { loading, error, request };
};
