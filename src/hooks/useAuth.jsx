import { useCallback, useEffect, useState } from 'react';

export const useAuth = () => {
	const [token, setToken] = useState(null);
	const [username, setUsername] = useState(null);

	const storageName = 'userData';

	const login = useCallback((jwtToken, user) => {
		setToken(jwtToken);
		setUsername(user);

		localStorage.setItem(
			storageName,
			JSON.stringify({ username: user, token: jwtToken })
		);
	}, []);

	const logout = useCallback(() => {
		setToken(null);
		setUsername(null);
		localStorage.removeItem(storageName);
	}, []);

	useEffect(() => {
		const data = JSON.parse(localStorage.getItem(storageName));
		if (data && data.token) {
			login(data.token, data.username);
		}
	}, [login]);

	return { login, logout, token, username };
};
